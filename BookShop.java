public class BookShop
{
String auther;
int price;
int pages;
public BookShop(String auther,int price,int pages)
{
	System.out.println("Hi i am constructor");
	this.auther=auther; //this refers to current object
	this.price=price;
	this.pages=pages;
}	
public static void main(String[] args){
	BookShop book1 = new BookShop("abc",20,55);
	//book1.auther="abc";
	//book1.price=20;
	//book1.pages=55;
	BookShop book2 = new BookShop("xyz",30,155);
	BookShop book3 = new BookShop("mnp",50,200);
	book1.buy();
	book2.buy();
	book3.buy();}
public void buy(){
	System.out.println(this.auther+"---"+this.price+"---"+this.pages);}
//called automatically when object is created.	
//No return dataType required.
//Should have same class name.
//Initializing object specific information.	

}