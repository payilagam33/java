public class BookShop1
{
//Default constructor	
public BookShop1(int no)
{
System.out.println("Hi i am 1-arg constructor");
}
//Zero arg constructor
public 	BookShop1()
{
System.out.println("Hi i am 0-arg constructor");	
}
public 	BookShop1(String str)
{
System.out.println("Hi i am 1-arg constructor");	
}	
public static void main(String[] args)
{
	BookShop1 book2 = new BookShop1(100);
	BookShop1 book = new BookShop1();
	BookShop1 book3 = new BookShop1("abc");
}
}

